# MyStorie

Aplicativo que estou desenvolvendo para demonstrar algumas práticas e conhecimentos que possuo.
Você pode entrar em contato comigo pelo e-mail: nicolaugalves@gmail.com

App I`m development to show some good practices and knowledge I have.
You can find me at email: nicolaugalves@gmail.com


- Aqui você poderá encontrar exemplos sobre:

- Android Annotation
- Bus event
- Retrofit
- checkstyle
- PMD
- findbugs
- Sonarqube
- Flavor
- Factory
- Abstract class
- Parse.com
- MVP ( Contract + Implementation )
- Mock / API Calls
- Infinity Scroll in RecyclerView ( In progress )
- Menu Lateral
- Arquitetura MVVM
- Roboeletrics / branch mvvm-jader
- Mockito - MVP
- MVP-ViewModel [ Ver branch Mvp-ViewModel ]
